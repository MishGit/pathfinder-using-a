﻿using UnityEngine;
using System.Collections;

public class MultiResScript : MonoBehaviour 
{
	//Camera
	private Camera cam;
	//Scaling 
	public static float height,width;
	public float resWidth,resHeight;
	public static float scaleFactor;
	public Transform obstacles;
	void Start () 
	{
		//----------------------------------------------------------------------------
		//Scaling purposes
		cam = Camera.main;
		height = Screen.height / 100.0f;
		width = Screen.width / 100.0f;
		scaleFactor = (height / resHeight)*100f; 
		Debug.Log ("Scale factor:" + scaleFactor);
		cam.orthographicSize = height / 2.0f;

		Debug.Log ("Height visible to the camera :" + height);
		Debug.Log ("Width of the visible area to the camera:" + width);

		//---------------------------------------------------------------------------------
		GameObject cube_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cube_1.transform.position = new Vector3 (0, 0, 0);
		cube_1.transform.SetParent (obstacles);
		Material newMat = Resources.Load("Cube", typeof(Material)) as Material;
		cube_1.GetComponent<Renderer> ().material = newMat;
		cube_1.layer = 8;
	}

}
